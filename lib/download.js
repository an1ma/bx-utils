/**
 * Created by mbakirov on 16.06.15.
 */

var request = require('request'),
    fs = require('fs'),
    path = require('path'),
    ProgressBar = require('progress');

module.exports = function (url, dest, callback)
{
    var
        req = request(url),
        bar,
        arAddress = url.split('http://').join('').split('https://').join('') .split('/'),
        filename = arAddress[ arAddress.length - 1 ]
        ;

    req
        .on('data', function (chunk) {
            bar = bar || new ProgressBar('Downloading '+filename+' [:bar] :percent :etas', {
                complete: '=',
                incomplete: ' ',
                width: 25,
                total: parseInt(req.response.headers['content-length'])
            });

            bar.tick(chunk.length);
        })
        .pipe(fs.createWriteStream(path.join(dest, filename)))
        .on('close', function (err) {
            bar.tick(bar.total - bar.curr);
        })
    ;
};