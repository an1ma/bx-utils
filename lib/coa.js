'use strict';

var Q = require('q'),
    CP = require('child_process'),
    PATH = require('path');

module.exports = require('coa').Cmd()
    .name(PATH.basename(process.argv[1]))
    .title(['Tools to work with bitrix', ''+
        'See http://anima.ru/ for more info.'].join('\n'))
    .helpful()
    .opt()
        .name('version').title('Show version').short('v').long('version').flag().only()
        .act(function() {
            return require('../package.json').version;
        })
    .end()
    .cmd().name('restore').apply(require('../components/restore')).end()
    .cmd().name('cache').apply(require('../components/cache')).end()
    .cmd().name('block').apply(require('../components/block')).end()
    .cmd().name('section').apply(require('../components/section')).end()
    .cmd().name('file').apply(require('../components/file')).end()
    //.cmd().name('component').apply(require('./component/component')).end()
    .completable()
    .act(function() {
        var defer = Q.defer(),
            readline = require('readline'),
            rl = readline.createInterface(process.stdin, process.stdout),
            prefix = '> ';
        rl.setPrompt(prefix, prefix.length);

        rl.on('line', function(line) {
            line = line.trim();
            if (!line) {
                rl.prompt();
                return;
            }
            var child = CP.spawn(process.argv[0],
                process.argv.slice(1, 2).concat(line.split(' ')),
                { cwd: process.cwd(), customFds: [-1, 1, 2] });
            child.on('exit', function() {
                rl.prompt();
            });
        })
            .on('close', function() {
                console.log('');
                process.stdin.destroy();
                defer.resolve();
            });

        /* jshint -W109 */
        console.log("Type '--help' for help, press ctrl+d or ctrl+c to exit");
        /* jshint +W109 */
        rl.prompt();

        return defer.promise;
    });
