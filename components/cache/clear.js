/**
 * Created by mbakirov on 15.06.15.
 */
/* jshint quotmark: false */

'use strict';

var Q = require('q'),
    path = require('path'),
    fs = require('fs'),
    rmdir = require('rmdir');

module.exports = function() {
    return this
        .title('Clear cache.').helpful()
        .opt()
            .name('component').short('c').long('component')
            .title('Clear component cache')
            .flag()
            .end()
        .opt()
            .name('css').short('s').long('styles')
            .title('Clear styles cache')
            .flag()
            .end()
        .opt()
            .name('js').short('j').long('js')
            .title('Clear javascripts cache')
            .flag()
            .end()
        .opt()
            .name('menu').short('m').long('menu')
            .title('Clear menu cache')
            .flag()
            .end()
        .opt()
            .name('managed').short('q').long('mysql')
            .title('Clear all mysql cache')
            .flag()
            .end()
        .opt()
            .name('bem').short('b').long('bem')
            .title('Clear all bem cache')
            .flag()
            .end()
        .opt()
            .name('all').short('a').long('all')
            .title('Clear all cache')
            .def(true)
            .flag()
            .end()
        .act(function(opts) {
            if(Object.keys(opts).length > 1)
            {
                delete opts.all;
            }

            var deleteDirs = [];

            for(var key in opts) {
                var opt = opts[key];
                if(!opt) continue;

                switch (key) {
                    case 'component':
                        deleteDirs.push('./bitrix/cache/s1');
                        break;
                    case 'css':
                        deleteDirs.push('./bitrix/cache/css');
                        break;
                    case 'js':
                        deleteDirs.push('./bitrix/cache/js');
                        break;
                    case 'menu':
                        deleteDirs.push('./bitrix/managed_cache/MYSQL/b_menu');
                        break;
                    case 'managed':
                        deleteDirs.push('./bitrix/managed_cache/MYSQL');
                        break;
                    case 'bem':
                        deleteDirs.push('./.bem/cache');
                        break;
                }
            }

            if(opts.all == true)
            {
                deleteDirs = [
                    './bitrix/cache/s1',
                    './bitrix/cache/css',
                    './bitrix/cache/js',
                    './bitrix/managed_cache/MYSQL',
                ];
            }

            for(var i = 0;i<deleteDirs.length;i++)
            {
                //dirName = files[f].replace(/\/$/, '');
                //console.log(dirName);
                rmdir(deleteDirs[i], function ( err, dirs, files ){
                    var deletedFiles = dirs.length;
                    deletedFiles += files.length;
                    console.log('Deleted '+deletedFiles+' object(s)');
                });
                //fs.unlink(path.resolve(deleteDirs[i]));
            }
            console.log('Cache cleared');
        });

};
