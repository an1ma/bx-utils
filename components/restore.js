/**
 * Created by mbakirov on 15.06.15.
 */

'use strict';

var Q = require('q'),
    PATH = require('path'),
    download = require('../lib/download');

module.exports = function() {
    var editions = [
        'start',
        'standart',
        'small_business',
        'business',
        'expert'
    ];
    return this
        .title('Restore utils')
        .helpful()
        //.cmd().name('file').apply(require('./restore/file')).end()

        .opt()
            .name('edition').short('e').long('edition')
            .title('Bitrix edition: start, standard, small_business, business, expert')
            .arr()
            .def(false)
            .end()
        .opt()
            .name('link').short('l').long('link')
            .title('Download backup by link')
            .arr()
            .def(false)
            .end()
        .act(function(opts, args) {
            var restoreDownloadUrl = 'http://www.1c-bitrix.ru/download/scripts/restore.php';

            if(opts.edition[0])
            {
                if(editions.indexOf(opts.edition[0]) >= 0)
                {
                    var coreDownloadUrl = 'http://www.1c-bitrix.ru/download/'+opts.edition+'_encode_php5.tar.gz';
                    download(coreDownloadUrl, PATH.resolve('./'));
                }
                else
                {
                    console.log('UNKNOWN EDITION: '+opts.edition);
                    console.log('Skiping core download');
                }
            }


            if(opts.link[0])
            {
                var backupDownloadUrl = opts.link[0];
                download(backupDownloadUrl, PATH.resolve('./'));
            }

            download(restoreDownloadUrl, PATH.resolve('./'));
        });
};
