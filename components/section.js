/**
 * Created by mbakirov on 15.06.15.
 */

'use strict';

var Q = require('q'),
    path = require('path'),
    inquirer = require('inquirer'),
    fs = require('fs'),
    glob = require("glob"),
    rmdir = require('rmdir'),
    BEM = require('bem').api;

module.exports = function() {

    return this
        .name(path.basename(process.argv[1]))
        .title('Operation with section tool.')
        .helpful()
        .cmd()
            .name('checkDir')
            .arg().name('path').title('section path, required').req().end()
            .act(function(opts, args){
                console.log('checkDir');
                console.log(args);
        })
        .end()
        .arg().name('block').title('block name, required').req().end()
        .opt().name('level').short('l').long('level').title('level name, default: local/blocks').def('local/blocks').end()
        .opt().name('elem').short('e').long('elem').title('element name').arr().end()
        .opt().name('mod').short('m').long('mod').title('modifier name').arr().end()
        .opt().name('val').short('v').long('val').title('modifier value').arr().end()
        .opt().name('forceTech').short('T').long('force-tech').title('use only specified tech').arr().end()
        .opt().name('force').short('f').long('force').title('force block delete').flag().end()
        .act(function(opts, args){
            var confirms = [],
                arDelete = [],
                arIgnore = [],
                blockDir = path.join(path.resolve(opts.level), args.block);

            if(opts.elem)
                for(var i = 0;i<opts.elem.length;i++)
                {
                    arDelete.push(path.join(blockDir, '__'+opts.elem[i]));
                }

            if(opts.mod)
            {
                for(var d=0,del=[];d<opts.mod.length;d++)
                {
                    for(var m=0,modPath;m<arDelete.length;m++)
                    {
                        modPath = path.join(arDelete[m], '_'+opts.mod[d]);
                        del.push(modPath);
                    }
                }
                if(del.length > 0) arDelete = del;
            }

            if(opts.val)
            {
                for(var d=0,del=[];d<opts.val.length;d++)
                {
                    for(var m=0,modValPath;m<arDelete.length;m++)
                    {
                        modValPath = arDelete[m].split(opts.level)[1].split('/').join('')+'_'+opts.val[d];
                        del.push(path.join(arDelete[m], modValPath));
                    }
                }
                if(del.length > 0) arDelete = del;
            }

            if(!opts.elem && !opts.mod && !opts.val)
            {
                if(!opts.forceTech && !opts.force)
                {
                    console.log('DELETING WHOLE BLOCK PREVENTED: For delete whole block use option -f or use -T for techs');
                    return;
                }
                else
                {
                    arDelete = [blockDir];
                }
            }

            if(opts.forceTech)
            {
                for(var d=0,del=[],delimeter;d<opts.forceTech.length;d++)
                {
                    delimeter = '/*.';
                    if(opts.val) delimeter = '.'

                    for(var m=0;m<arDelete.length;m++)
                    {
                        del.push(arDelete[m]+delimeter+opts.forceTech[d]);
                        if(opts.forceTech[d] == 'js')
                        {
                            arIgnore.push(arDelete[m]+delimeter+'deps.js');
                        }
                    }
                }
                if(del.length > 0) arDelete = del;
            }
            else
            {
                for(var m=0;m<arDelete.length;m++)
                {
                    delimeter = '/';
                    if(opts.val) delimeter = '*'
                    arDelete[m] = arDelete[m]+delimeter;
                }
            }

            var strFiles = '';
            for(var m=0;m<arDelete.length;m++)
            {
                strFiles += arDelete[m]+"\n";
            }

            inquirer.prompt([{
                    type: 'confirm',
                    name: 'deleteFiles',
                    message: "Delete files by mask(s): \n"+strFiles+'?',
                    default: 'n',
                    choices: ['n', 'Y']
                }],
                function( answers ) {
                    var deletedFiles = 0,
                        files,
                        dirName;

                    if(answers.deleteFiles)
                    {
                        for(var m=0;m<arDelete.length;m++)
                        {
                            files = glob.sync(arDelete[m], {dot:true, ignore:arIgnore});
                            for(var f=0;f<files.length;f++)
                            {
                                if(files[f].substr(-1) == '/')
                                {
                                    dirName = files[f].replace(/\/$/, '');
                                    //console.log(dirName);
                                    rmdir(dirName, function ( err, dirs, files ){
                                        deletedFiles += dirs.length;
                                        deletedFiles += files.length;
                                        console.log('Deleted '+deletedFiles+' object(s)');
                                    });
                                }
                                else
                                {
                                    fs.unlink(files[f]);
                                    deletedFiles++;
                                }
                            }
                        }
                        if(!dirName) console.log('Deleted '+deletedFiles+' object(s)');
                    }
                });
        })
        .end()
        //.cmd().name('mv').apply(require('./block/mv')).end()
        .arg().name('block').title('block name, required').req().end()
        .opt().name('level').short('l').long('level').title('level name, default: local/blocks').def('local/blocks').end()
        .opt().name('elem').short('e').long('elem').title('element name').arr().end()
        .opt().name('mod').short('m').long('mod').title('modifier name').arr().end()
        .opt().name('val').short('v').long('val').title('modifier value').arr().end()
        .opt().name('addTech').short('t').long('add-tech').title('add tech').arr().end()
        .opt().name('forceTech').short('T').long('force-tech').title('use only specified tech').arr().end()
        .opt().name('noTech').short('n').long('no-tech').title('exclude tech').arr().end()
        .act(function(opts, args) {
            opts.block = args.block;

            Q.when(BEM.create(opts), function() {
                console.log('Block `%s` created', args.block);
            });
        })
};
