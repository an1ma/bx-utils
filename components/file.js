/**
 * Created by mbakirov on 15.06.15.
 */

'use strict';

var Q = require('q'),
    path = require('path'),
    inquirer = require('inquirer'),
    fs = require('fs'),
    glob = require("glob"),
    rmdir = require('rmdir'),
    mkdirp = require('mkdirp'),
    BX = require('../lib/coa');

module.exports = function() {

    return this
        .title('Operation with files tool.')
        .helpful()
        //.cmd().name('rm')//.apply(require('./block/rm'))
        .arg().name('path').title('file path, required').req().end()
        .opt().name('title').short('t').long('title').title('file public title').def('Title').end()
        .opt().name('page_template').short('p').long('page-template').title('page template file, default is local/templates/main/page_templates/standart.php').def('local/templates/main/page_templates/standart.php').end()
        .act(function(opts, args){
            console.log(opts);
            console.log(args);

            console.log(BX);

            var arPath = args.path.split('/'),
                directoryPath, fileName;

            directoryPath = arPath.splice(0, arPath.length-1).join('/').replace(/^\//, '');
            fileName = arPath.splice(arPath.length-1, 1)[0];

            if(!/\.php$/.test(fileName)) fileName += '.php';

            // check if file exists opts.page_template

            //BX.section.checkDir(directoryPath);

            var templatePath = path.resolve(opts.page_template),
                newFilePath = path.resolve(path.join(directoryPath, fileName));

            try {
                var stats = fs.lstatSync(directoryPath);
                if (stats.isDirectory()) {
                    console.log('is directory');
                }
            }
            catch (e) {
                mkdirp(directoryPath);
                //console.log('no such directory');
            }

            console.log(templatePath);
            console.log(newFilePath);

            //fs.createReadStream(path.resolve(templatePath)).pipe(fs.createWriteStream(newFilePath));
            //console.log(directoryPath);
            //console.log(directoryPath);
            //console.log(fileName);


        })
};
